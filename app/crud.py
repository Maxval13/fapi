from sqlalchemy.orm import Session
from model import Fapi
from shema import FapiSchema

# Получение всех данных
def get_fapi(db:Session, skip:int=0, limit:int=100):
    return db.query(Fapi).offset(skip).limit(limit).all()

# Получение данных по id
def get_fapi_by_id(db:Session, fapi_id: int):
    return db.query(Fapi).filter(Fapi.id == fapi_id).first()

# Создание записи в БД
def create_fapi(db:Session, fapi: FapiSchema):
    _fapi = Fapi(name=fapi.name,gender=fapi.gender)
    db.add(_fapi)
    db.commit()
    db.refresh(_fapi)
    return _fapi

# Удаление записи из БД
def remove_fapi(db:Session, fapi_id: int):
    _fapi = get_fapi_by_id(db=db, fapi_id=fapi_id)
    db.delete(_fapi)
    db.commit()

# Обновить данные в БД
def update_fapi(db:Session, fapi_id:int, name:str, gender:str):
    _fapi = get_fapi_by_id(db=db,fapi_id=fapi_id)
    _fapi.name = name
    _fapi.gender = gender
    db.commit()
    db.refresh(_fapi)
    return _fapi
