from config import Base
from sqlalchemy import Column, Integer, String


class Fapi(Base):
    __tablename__ = "fapi"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    gender = Column(String)
