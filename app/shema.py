from typing import List, Optional, Generic, TypeVar
from pydantic import BaseModel, Field
from pydantic.generics import GenericModel

T = TypeVar('T')

class FapiSchema(BaseModel):
    id: Optional[int]=None
    name: Optional[str]=None
    gender: Optional[str]=None

    class Config:
        orm_mode = True


class RequestFapi(BaseModel):
    parameter: FapiSchema = Field(...)

class Response (GenericModel, Generic[T]):
    code: str
    status: str
    message: str
    result: Optional[T]
