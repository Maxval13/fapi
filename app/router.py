from fastapi import APIRouter, HTTPException, Path, Depends
from config import SessionL
from sqlalchemy.orm import Session
from shema import FapiSchema, RequestFapi, Response
import crud

router = APIRouter()

def get_db():
    db = SessionL()
    try:
        yield db
    finally:
        db.close()


@router.post('/create')
async def create(request:RequestFapi, db:Session=Depends(get_db)):
    crud.create_fapi(db, fapi=request.parameter)
    return Response(code=200, status="OK",message="record created successfully").dict(exclude_none=True)

@router.get("/")
async def get(db:Session=Depends(get_db)):
    _fapi = crud.get_fapi(db,0,100)
    return Response(code=200, status="OK", message="data extracted successfully", result=_fapi).dict(exclude_none=True)

@router.get("/{id}")
async def get_by_id(id:int,db:Session = Depends(get_db)):
    _fapi = crud.get_fapi_by_id(db,id)
    return Response(code=200, status="OK", message="success get data", result=_fapi).dict(exclude_none=True)

@router.post("/update")
async def update_fapi(request: RequestFapi, db:Session = Depends(get_db)):
    _fapi = crud.update_fapi(db, fapi_id=request.parameter.id,name=request.parameter.name,gender=request.parameter.gender)
    return Response(code=200, status="OK", message="success update data", result=_fapi)

@router.delete("/{id}")
async def delate_fapi(id:int, db:Session = Depends(get_db)):
    crud.remove_fapi(db, fapi_id=id)
    return Response(code=200, status="OK", message="success delete data").dict(exclude_none=True)

